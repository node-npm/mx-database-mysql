"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = require("../src/mysql");
let mcon = new mysql_1.TeMysql();
describe('数据库测试', () => {
    before(function () {
        return mcon.init("10.0.0.10", 3306, "test", "root", "hello123");
    });
    it("查询", function () {
        return new Promise(function (reslove, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                let unit = mcon.get_list("shop", {});
                yield unit.load(undefined, { time: 1 });
                console.log(unit.data);
                reslove();
            });
        });
    });
    it("插入", function () {
        return mcon.insert("shop", { name: "时间" + Date.now() });
    });
    it("修改时间", function () {
        return new Promise(function (reslove, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                let unit = mcon.get_unit("shop", {});
                yield unit.load(undefined, { time: 1 });
                console.log(unit.data);
                unit.set("time", new Date);
                yield unit.force_save();
                reslove();
            });
        });
    });
    it("BUFFER", function () {
        return new Promise(function (reslove, reject) {
            return __awaiter(this, void 0, void 0, function* () {
                let unit = mcon.get_unit("shop", { date: null });
                yield unit.load(undefined, { time: 1 });
                console.log(unit.data.bin.toString());
                unit.setSafe("bin", Buffer.from("你好"));
                yield unit.force_save();
                reslove();
            });
        });
    });
    after(function () {
        return mcon.close();
    });
});
//# sourceMappingURL=mysql.js.map