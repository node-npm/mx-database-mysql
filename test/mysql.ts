import { TeMysql } from "../src/mysql";

let mcon = new TeMysql()
describe('数据库测试', () => {
    before(function () {
        return mcon.init("10.0.0.10", 3306, "test", "root", "hello123")
    })

    it("查询", function () {
        return new Promise<void>(async function (reslove, reject) {
            let unit = mcon.get_list("shop", {})
            await unit.load(undefined, { time: 1 })
            console.log(unit.data)
            reslove()
        })
    })

    it("插入", function () {
        return mcon.insert("shop", { name: "时间" + Date.now() })
    })

    it("修改时间", function () {
        return new Promise<void>(async function (reslove, reject) {
            let unit = mcon.get_unit("shop", {})
            await unit.load(undefined, { time: 1 })

            console.log(unit.data)
            unit.set("time", new Date)
            await unit.force_save()
            reslove()
        })
    })

    it("BUFFER", function () {
        return new Promise<void>(async function (reslove, reject) {
            let unit = mcon.get_unit<any>("shop", {date:null})
            await unit.load(undefined, { time: 1 })

            console.log(unit.data.bin.toString())
            unit.setSafe("bin", Buffer.from("你好"))
            await unit.force_save()
            reslove()
        })
    })


    after(function () {
        return mcon.close()
    })
});