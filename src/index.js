"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MysqlMoudle = void 0;
const mysql_1 = require("./mysql");
var s_ready = false;
var s_mongodb = new mysql_1.TeMysql();
exports.MysqlMoudle = {
    name: 'MysqlMoudle',
    regist_colltion: function (database, name, indexs) {
        // 暂时无效，等以后考虑 后续可能提供表格调整语句，然后提示异常
    },
    regist_function: function (database, func) {
        // 暂时无效，等以后考虑 后续可能提供表格调整语句，然后提示异常
    },
    /**
     * 获取数据库 一般认为都是初始化过的
     * @param database
     */
    get_database: function (database) {
        if (s_mongodb.client.config.database != database) {
            // 这里增加一个警告
            console.warn("database is error " + database);
        }
        return s_mongodb;
    },
    /**
     * 帐号服务器数据库初始化
     * @param configs
     */
    init: function (configs) {
        return __awaiter(this, void 0, void 0, function* () {
            if (s_ready)
                return true;
            yield s_mongodb.init(configs.host, configs.port, configs.database, configs.user, configs.passwd);
            s_ready = true;
            return true;
        });
    }
};
//# sourceMappingURL=index.js.map