"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeMysql = void 0;
const base_1 = require("./base");
const MyUnit_1 = require("./MyUnit");
const MyList_1 = require("./MyList");
class TeMysql extends base_1.MysqlConn {
    // 插入数据
    update(table, find, info, insert) {
        return __awaiter(this, void 0, void 0, function* () {
            let unit = MyUnit_1.GetUnit(this, table, find);
            yield unit.load();
            if (unit.empty) {
                this.insert(table, Object.assign(find, info));
            }
            else {
                for (let key in info)
                    unit.set(key, info[key]);
                yield unit.force_save();
            }
            return true;
        });
    }
    del(table, find) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.delete(table, find);
        });
    }
    get_unit(table, find) {
        return MyUnit_1.GetUnit(this, table, find);
    }
    get_list(table, find) {
        return MyList_1.GetList(this, table, find);
    }
}
exports.TeMysql = TeMysql;
//# sourceMappingURL=mysql.js.map