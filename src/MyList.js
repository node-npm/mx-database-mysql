"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetList = void 0;
const base_1 = require("./base");
const Util_1 = require("../lib/Util");
class List {
    constructor(db, table, findKey) {
        this._data = [];
        this._start = 0;
        this._limit = 0;
        this.mydb = db;
        this.table = table;
        this.findKey = findKey;
    }
    get data() {
        return this._data;
    }
    get length() {
        return this._data.length;
    }
    // 深拷贝一份数据
    clone() {
        return Util_1.Util.copy(this._data);
    }
    limit(num) {
        this._limit = num;
        return this;
    }
    skip(num) {
        this._start = num;
        return this;
    }
    get_at(index, org = false) {
        if (index < 0 || index >= this._data.length)
            return null;
        if (org)
            return this._data[index];
        return Util_1.Util.copy(this._data[index]);
    }
    rm_at(index) {
        return __awaiter(this, void 0, void 0, function* () {
            if (index < 0 || index >= this._data.length)
                return;
            var rp = this._data.splice(index, 1);
            for (var i = 0; i < rp.length; i++) {
                yield this.mydb.deleteOnce(this.table, rp);
            }
            return;
        });
    }
    clear_all() {
        return __awaiter(this, void 0, void 0, function* () {
            while (this.length > 0) {
                yield this.rm_at(0);
            }
        });
    }
    _has_same(v, fobj) {
        var bSame = true;
        for (var key in fobj) {
            if (v[key] != fobj[key]) {
                bSame = false;
                break;
            }
        }
        return bSame;
    }
    del(fObj) {
        var idx = -1;
        for (var i = 0; i < this._data.length; i++) {
            var c = this._data[i];
            if (this._has_same(c, fObj)) {
                idx = i;
                break;
            }
        }
        // var idx = this._data_.indexOf(v);
        if (idx >= 0)
            this.rm_at(idx);
    }
    load(filter, sort) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let show = [];
                filter = filter || {};
                let keys = Object.keys(filter);
                if (keys.length > 0) {
                    if (filter[keys[0]] == 0) {
                        // 这里是屏蔽模式
                        let listInfo = this.mydb.tableFields[this.table];
                        if (listInfo)
                            for (let key in listInfo)
                                if (filter[key] == undefined)
                                    show.push(key);
                    }
                    else {
                        show = keys;
                    }
                }
                let qStr = `select ${show.length == 0 ? '*' : show.join(',')} from ${this.table} `;
                if (Object.keys(this.findKey).length > 0) {
                    qStr += `where ${base_1.toWhere(this.findKey)}`;
                }
                if (sort) {
                    let str = " order by ";
                    for (let key in sort) {
                        str += ` ${key} ${(sort[key] == -1) ? "Desc" : ""}`;
                    }
                    qStr += str;
                }
                if (this._limit != 0 && this._start != 0) {
                    qStr += ` limit ${this._start},${this._limit}`;
                }
                else if (this._limit != 0) {
                    qStr += ` limit ${this._limit}`;
                }
                else if (this._start != 0) {
                    qStr += ` limit ${this._start},-1`;
                }
                let list = yield this.mydb.query(qStr);
                if (list.length > 0) {
                    this._data = list;
                }
                else {
                    this._data = [];
                }
            }
            catch (e) {
                throw e;
            }
            return this;
        });
    }
    insert(info) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.mydb.insert(this.table, info);
            this._data.push(info);
            return result;
        });
    }
}
function GetList(db, table, findKey) {
    return new List(db, table, findKey);
}
exports.GetList = GetList;
//# sourceMappingURL=MyList.js.map