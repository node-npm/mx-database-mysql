import { MysqlConn } from "./base";
import { GetUnit } from "./MyUnit";
import { GetList } from "./MyList";


export class TeMysql extends MysqlConn {
    // 插入数据
    async update(table: string, find: { [x: string]: any }, info: { [x: string]: any }, insert?: boolean) {
        let unit = GetUnit(this, table, find);
        await unit.load();
        if (unit.empty) {
            this.insert(table, Object.assign(find, info))
        }
        else {
            for (let key in info) unit.set(key, info[key])
            await unit.force_save()
        }
        return true
    }

    async del(table: string, find: { [x: string]: any }) {
        return this.delete(table, find);
    }

    get_unit<T>(table: string, find: { [x: string]: any }) {
        return GetUnit<T>(this, table, find)
    }

    get_list<T>(table: string, find: { [x: string]: any }) {
        return GetList<T>(this, table, find)
    }
}
