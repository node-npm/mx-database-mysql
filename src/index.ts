import { TeMysql } from "./mysql";

var s_ready = false;

var s_mongodb: TeMysql = new TeMysql();
export var MysqlMoudle = {
    name: 'MysqlMoudle',
    regist_colltion: function (database: string, name: string, indexs: string[]) {
        // 暂时无效，等以后考虑 后续可能提供表格调整语句，然后提示异常
    },

    regist_function: function (database: string, func: Function) {
        // 暂时无效，等以后考虑 后续可能提供表格调整语句，然后提示异常
    },

    /**
     * 获取数据库 一般认为都是初始化过的
     * @param database 
     */
    get_database: function (database: string) {
        if (s_mongodb.client.config.database != database) {
            // 这里增加一个警告
            console.warn("database is error " + database)
        }

        return s_mongodb
    },

    /**
     * 帐号服务器数据库初始化
     * @param configs 
     */
    init: async function (configs: { host: string, port: number, database: string, user: string, passwd: string, connectTimeout?: number, timezone?: number }) {
        if (s_ready) return true;
        await s_mongodb.init(configs.host, configs.port, configs.database, configs.user, configs.passwd)
        s_ready = true;
        return true;
    }
}