"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Util = void 0;
const promise_1 = require("./promise");
const path_1 = __importDefault(require("path"));
// 拷贝函数 是否包括函数拷贝
function func_copy(obj, bFunc = false) {
    var out;
    if (obj instanceof Array) {
        out = [];
        // 数组这里就
        for (let i = 0; i < obj.length; i++) {
            out[i] = func_copy(obj[i], bFunc);
        }
    }
    else if (obj instanceof Buffer) {
        // buffer 这里就不拷贝算了
        out = Buffer.from(obj);
    }
    else if (obj instanceof Date) {
        out = new Date(obj);
    }
    else if (typeof obj == 'object') {
        out = {};
        for (var key in obj) {
            if (key == 'clone' || key == 'global') {
                continue;
            }
            if (typeof obj[key] == 'function' && !bFunc) {
                continue;
            }
            if (obj[key] == null) {
                out[key] = null;
            }
            else
                out[key] = func_copy(obj[key], false);
        }
    }
    else {
        out = obj;
    }
    return out;
}
function func_attach(dst, src) {
    if (!dst || !src)
        return;
    for (var key in src) {
        dst[key] = exports.Util.copy(src[key]);
    }
    return dst;
}
function promise_setInterval(...args) {
    let [name, callback, time] = args;
    if (typeof name != 'string') {
        name = '';
        callback = args[0];
        time = args[1];
    }
    let handle;
    function update() {
        promise_1.createPromise(callback()).finally(function () {
            if (handle) {
                if (handle.refresh) {
                    handle.refresh();
                }
                else {
                    handle = setTimeout(update, time);
                }
            }
        });
    }
    handle = setTimeout(update, time);
    handle['name'] = name;
    return {
        stop: function () {
            clearTimeout(handle);
        }
    };
}
function getCallerFileNameAndLine() {
    function getException() {
        try {
            throw Error('');
        }
        catch (err) {
            return err;
        }
    }
    const err = getException();
    const stack = err.stack;
    const stackArr = stack.split('\n');
    let callerLogIndex = 0;
    for (let i = 0; i < stackArr.length; i++) {
        if (stackArr[i].indexOf('makeError') > 0 && i + 1 < stackArr.length) {
            callerLogIndex = i + 1;
            break;
        }
    }
    if (callerLogIndex !== 0) {
        const callerStackLine = stackArr[callerLogIndex];
        let list = callerStackLine.split(/\(|\)/);
        if (list[1] && list[1].length > 0) {
            return list[1].replace(process.cwd(), '').replace(/\\/g, "/").replace('/', '');
        }
        else {
            return `[${callerStackLine.substring(callerStackLine.lastIndexOf(path_1.default.sep) + 1, callerStackLine.lastIndexOf(':'))}]`;
        }
    }
    else {
        return '[-]';
    }
}
exports.Util = {
    copy: func_copy,
    attach: func_attach,
    setInterval: promise_setInterval,
    makeError: function (code, msg) {
        return { code: code, errMsg: msg, __line__: getCallerFileNameAndLine() };
    }
};
//# sourceMappingURL=Util.js.map