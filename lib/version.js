"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.V_UnderV8 = void 0;
function toNumbers(s) {
    let ls = s.split(".");
    let nums = [];
    for (let i = 0; i < ls.length; i++) {
        nums.push(parseInt(ls[i]));
    }
    return nums;
}
// >=
function gecmp(a, b) {
    let numAs = toNumbers(a);
    let numBs = toNumbers(b);
    for (let i = 0; i < Math.max(numAs.length, numBs.length); i++) {
        let na = numAs[i] || 0;
        let nb = numBs[i] || 0;
        if (na > nb)
            return true;
        if (na < nb)
            return false;
    }
    return true;
}
exports.V_UnderV8 = !gecmp(process.versions.node, "9.0.0");
//# sourceMappingURL=version.js.map