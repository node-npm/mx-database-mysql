"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.execPools2 = exports.execPools = void 0;
const promise_1 = require("./promise");
/**
 * 提供执行不同处理过程的协程
 * 返回只有 resolve的方式，没有 reject的执行
 * @param pools 执行程序池子
 * @param threadNum 启动的执行协程数量
 * @param args 每个方法执行的时候使用相同的参数组
 */
function execPools(pools, threadNum, ...args) {
    let running_thread = [];
    for (let i = 0; i < threadNum; i++) {
        running_thread.push([]);
    }
    // 把内容水平分组
    for (let i = 0; i < pools.length; i++) {
        running_thread[i % threadNum].push(pools[i]);
    }
    return new Promise(function (resolve) {
        let count = 0;
        let succPool = [];
        let failPool = [];
        for (let i = 0; i < threadNum; i++) {
            promise_1.createPromise(exec(i, threadNum, running_thread[i], ...args))
                .then(function (result) {
                failPool.push(...result.fail);
                succPool.push(...result.succ);
            })
                .finally(function () {
                count++;
                if (count >= threadNum) {
                    resolve({ succ: succPool, fail: failPool });
                }
            });
        }
    });
}
exports.execPools = execPools;
function exec(theadIdx, threadNum, pools, ...args) {
    return __awaiter(this, void 0, void 0, function* () {
        let succ = [];
        let fail = [];
        for (let i = 0; i < pools.length; i++) {
            let idx = theadIdx + i * threadNum;
            try {
                yield pools[i](...args);
                succ.push(idx);
            }
            catch (e) {
                fail.push(idx);
            }
        }
        return { succ: succ, fail: fail };
    });
}
/**
 * 执行相同处理过程的协程
 * 返回只有 resolve的方式，没有 reject的执行
 * @param exec 执行方法
 * @param threadNum 协程数量
 * @param args 执行的数据池子
 */
function execPools2(exec, threadNum, ...args) {
    let running_thread = [];
    for (let i = 0; i < threadNum; i++) {
        running_thread.push([]);
    }
    // 把内容水平分组
    for (let i = 0; i < args.length; i++) {
        running_thread[i % threadNum].push(args[i]);
    }
    return new Promise(function (resolve) {
        let count = 0;
        let succPool = [];
        let failPool = [];
        for (let i = 0; i < threadNum; i++) {
            promise_1.createPromise(exec2(i, threadNum, exec, ...running_thread[i]))
                .then(function (result) {
                failPool.push(...result.fail);
                succPool.push(...result.succ);
            })
                .finally(function () {
                count++;
                if (count >= threadNum) {
                    resolve({ succ: succPool, fail: failPool });
                }
            });
        }
    });
}
exports.execPools2 = execPools2;
function exec2(theadIdx, threadNum, run_exec, ...args) {
    return __awaiter(this, void 0, void 0, function* () {
        let succ = [];
        let fail = [];
        for (let i = 0; i < args.length; i++) {
            let idx = theadIdx + i * threadNum;
            try {
                yield run_exec(args[i]);
                succ.push(idx);
            }
            catch (e) {
                fail.push(idx);
            }
        }
        return { succ: succ, fail: fail };
    });
}
//# sourceMappingURL=exec_pools.js.map