"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createPromise = void 0;
const version_1 = require("./version");
// 制作一个自己封装的promise 主要解决node8上面没有finally
class TePromise {
    constructor(cb) {
        this.thenPools = [];
        this.catchPools = [];
        this.finallPools = [];
        if (cb instanceof Promise) {
            this.pm = cb;
        }
        else {
            this.pm = new Promise(cb);
        }
        this.pm.then(this.onThen.bind(this)).catch(this.onCatch.bind(this));
        this.pm.then = this.then.bind(this);
        this.pm.catch = this.catch.bind(this);
        this.pm.finally = this.finally.bind(this);
    }
    onThen(v) {
        // 这里按照列表一个一个来出现异常就进入catch
        try {
            var fn = this.thenPools.shift();
            while (fn) {
                fn(v);
                fn = this.thenPools.shift();
            }
        }
        catch (e) {
            this.onCatch(e);
        }
        finally {
            this.doFinally();
        }
    }
    onCatch(e) {
        try {
            var fn = this.catchPools.shift();
            while (fn) {
                fn(e);
                fn = this.catchPools.shift();
            }
        }
        catch (e) {
        }
        finally {
            this.doFinally();
        }
    }
    doFinally() {
        var fn = this.finallPools.shift();
        while (fn) {
            fn();
            fn = this.finallPools.shift();
        }
    }
    then(onReslove, onReject) {
        this.thenPools.push(onReslove);
        if (onReject)
            this.catchPools.push(onReject);
        return this;
    }
    catch(cb) {
        this.catchPools.push(cb);
        return this;
    }
    finally(cb) {
        this.finallPools.push(cb);
        return this;
    }
}
function createPromise(func) {
    if (version_1.V_UnderV8) {
        return (new TePromise(func)).pm;
    }
    else {
        if (func instanceof Promise)
            return func;
        else
            return new Promise(func);
    }
}
exports.createPromise = createPromise;
//# sourceMappingURL=promise.js.map